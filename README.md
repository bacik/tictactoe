# TicTacToe
My new project completed as of **10th September 2020.**  
Java TicTacToe project using **JavaFX** and tested using **JUnit** and **TestFX** libraries.

**To play the game** - after clicking run on the application the input view is shown. Then input the values of the board size (from **3 to 21**) and the number of winning symbols (from **3 to 21** but has to be equal to or smaller than the board size).  
Once you input the correct values the game starts. The game automatically switches between players X and O, player X being the first to play in every game. The current player is shown in the top portion of the window above the playing board.
After one of the players wins, the board needs to be reset by clicking on the Reset game button. When you want to play again with different (or even the same) values, click on the Back to the main menu button and input the values again.
  


TicTacToe board can be created with custom size and custom number of winning symbols that are inputted in the Input view.  
After inputting correct values the view switches to Game view. Incorrect values always show error dialog - incorrect in this case means the values are not within the interval from 3 to 21, or are not numbers. The board size has to be the same or bigger than the number of winning symbols. The maximum size of the board is because of the board fits maximum 21 buttons on a full screen on 1920*1080 resolution.