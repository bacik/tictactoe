package tictactoe;

public class Game {
    private String[][] board;
    private int boardSize;
    private int winsOn;
    //false means player X
    private boolean playingPlayer = false;
    private String currentPlayerSymbol = "X";
    private int moveCounter = 0;
    private boolean gameOver = false;
    private int pointsX = 0;
    private int pointsO = 0;

    public Game() {
    }

    public String[][] getBoard() {
        return board;
    }

    public int getWinsOn() {
        return winsOn;
    }

    public void setWinsOn(int winsOn) {
        this.winsOn = winsOn;
    }

    public void setPlayingPlayer(boolean playingPlayer) {
        this.playingPlayer = playingPlayer;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public int getPointsX() {
        return pointsX;
    }

    public void addPointX() {
        this.pointsX++;
    }

    public int getPointsO() {
        return pointsO;
    }

    public void addPointO() {
        this.pointsO++;
    }

    public String getCurrentPlayerSymbol() {
        return currentPlayerSymbol;
    }

    public void changeCurrentPlayerSymbol() {
        this.currentPlayerSymbol = playingPlayer ? "O" : "X";
    }

    public int getBoardSize() {
        return boardSize;
    }

    public void createBoard(int size) {
        this.board = new String[size][size];
        this.boardSize = size;
        resetBoard();
    }

    public void addSymbol(int x, int y) {
        if (!gameOver && board[x][y].equals("")) {
            board[x][y] = currentPlayerSymbol;
            playerMove();
        }
    }

    public void playerMove() {
        playingPlayer = !playingPlayer;
        moveCounter++;
    }

    public boolean playerWinCheck(int x, int y) {
        int numberOfIterations = 0;
        int symbolCounter = 0;
        for (int i = x - (winsOn - 1); i <= x + (winsOn - 1); ) {
            if (i < 0) {
                i++;
                continue;
            }
            if (i > boardSize - 1) {
                break;
            }
            if (board[i][y].equals(currentPlayerSymbol)) {
                symbolCounter++;
                if (symbolCounter == winsOn) {
                    return addPoint(currentPlayerSymbol);
                }
                i++;
            } else {
                numberOfIterations++;
                if (numberOfIterations == winsOn) {
                    break;
                }
                i = x - (winsOn - 1) + numberOfIterations;
                symbolCounter = 0;
            }
        }

        numberOfIterations = 0;
        symbolCounter = 0;
        for (int i = y - (winsOn - 1); i <= y + (winsOn - 1); ) {
            if (i < 0) {
                i++;
                continue;
            }
            if (i > boardSize - 1) {
                break;
            }
            if (board[x][i].equals(currentPlayerSymbol)) {
                symbolCounter++;
                if (symbolCounter == winsOn) {
                    return addPoint(currentPlayerSymbol);
                }
                i++;
            } else {
                numberOfIterations++;
                if (numberOfIterations == winsOn) {
                    break;
                }
                i = y - (winsOn - 1) + numberOfIterations;
                symbolCounter = 0;
            }
        }

        numberOfIterations = 0;
        symbolCounter = 0;
        for (int i = x - (winsOn - 1), j = y - (winsOn - 1); i <= x + (winsOn - 1); ) {
            if (i < 0 || j < 0) {
                i++;
                j++;
                continue;
            }
            if (i > boardSize - 1 || j > boardSize - 1) {
                break;
            }
            if (board[i][j].equals(currentPlayerSymbol)) {
                symbolCounter++;
                if (symbolCounter == winsOn) {
                    return addPoint(currentPlayerSymbol);
                }
                i++;
                j++;
            } else {
                numberOfIterations++;
                if (numberOfIterations == winsOn) {
                    break;
                }
                i = x - (winsOn - 1) + numberOfIterations;
                j = y - (winsOn - 1) + numberOfIterations;
                symbolCounter = 0;
            }
        }

        numberOfIterations = 0;
        symbolCounter = 0;
        for (int i = x + (winsOn - 1), j = y - (winsOn - 1); i <= x + (winsOn - 1); ) {
            if (i < 0 || j > boardSize - 1) {
                break;
            }
            if (i > boardSize - 1 || j < 0) {
                i--;
                j++;
                continue;
            }
            if (board[i][j].equals(currentPlayerSymbol)) {
                symbolCounter++;
                if (symbolCounter == winsOn) {
                    return addPoint(currentPlayerSymbol);
                }
                i--;
                j++;
            } else {
                numberOfIterations++;
                if (numberOfIterations == winsOn) {
                    break;
                }
                i = x + (winsOn - 1) - numberOfIterations;
                j = y - (winsOn - 1) + numberOfIterations;
                symbolCounter = 0;
            }
        }
        changeCurrentPlayerSymbol();
        return false;
    }

    public boolean tieCheck() {
        if (moveCounter == boardSize * boardSize && !gameOver) {
            gameOver = true;
            return true;
        }
        return false;
    }

    public void resetBoard() {
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                board[i][j] = "";
            }
        }
        playingPlayer = false;
        moveCounter = 0;
        gameOver = false;
        changeCurrentPlayerSymbol();
    }

    public void resetScore() {
        resetBoard();
        pointsX = 0;
        pointsO = 0;
    }

    public boolean addPoint(String text) {
        gameOver = true;
        if (text.equals("X")) {
            addPointX();
        } else if (text.equals("O")) {
            addPointO();
        }
        return true;
    }
}
