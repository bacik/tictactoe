package tictactoe;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

public class TicTacToeApplication extends Application {
    private final Game game = new Game();
    private Button[] buttons;
    private Button resetButton;
    private Button backToMainMenuButton;
    private Stage stage;

    public TicTacToeApplication() {
    }

    public static void main(String[] args) {
        launch(TicTacToeApplication.class);
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("TicTacToe");
        stage.setScene(createInputScene());
        stage.show();
        this.stage = stage;
    }

    public void setGameButtonSymbol(Button button) {
        if (!button.getText().equals("X") && !button.getText().equals("O")) {
            button.setText(game.getCurrentPlayerSymbol());
        }
    }

    public Label createView() {
        Label label = new Label("Turn: " + game.getCurrentPlayerSymbol());
        label.setId("turnLabel");
        labelTextAlign(label);
        return label;
    }

    public void clickGameButton(Button button, BorderPane layout, int x, int y) {
        button.setOnAction((event) -> {
            if (!game.isGameOver()) {
                setGameButtonSymbol(button);
                game.addSymbol(x, y);
                if (game.playerWinCheck(x, y)) {
                    Label label = new Label("Player " + game.getCurrentPlayerSymbol() + " wins!");
                    label.setId("gameOverLabel");
                    labelTextAlign(label);
                    layout.setTop(label);
                    layout.setBottom(createButtonsVBoxForGameView());
                }
                if (game.tieCheck()) {
                    Label label = new Label("Tie!");
                    label.setId("tieLabel");
                    labelTextAlign(label);
                    layout.setTop(label);
                }
                if (!game.isGameOver()) {
                    layout.setTop(createView());
                }
            }
        });
    }

    public void labelTextAlign(Label label) {
        label.setFont(Font.font("Monospaced", 40));
        label.setPadding(new Insets(5));
        label.setAlignment(Pos.CENTER);
        label.setMaxWidth(Double.MAX_VALUE);
        label.setTextAlignment(TextAlignment.CENTER);
    }

    public void inputLabelTextAlign(Label label) {
        label.setFont(Font.font("MS Shell Dlg", 13));
        label.setPadding(new Insets(5));
        label.setAlignment(Pos.CENTER_LEFT);
        label.setMaxWidth(Double.MAX_VALUE);
        label.setTextAlignment(TextAlignment.LEFT);
        label.setPadding(new Insets(15, 10, 10, 15));
    }

    public void reset(Button[] buttons, Button button, BorderPane layout) {
        button.setOnAction((event) ->
        {
            game.resetBoard();
            for (Button value : buttons) {
                value.setText(" ");
            }
            layout.setTop(createView());
        });
    }

    public void backToMainMenu(Button[] buttons, Button button) {
        button.setOnAction((event) ->
        {
            game.resetBoard();
            for (Button value : buttons) {
                value.setText(" ");
            }
            stage.setScene(createInputScene());
            game.resetScore();
        });
    }

    public Button createBackToMainMenuButton() {
        Button button = new Button();
        button.setFont(Font.font("Monospaced", 20));
        button.setText("Back to main menu");
        button.setPadding(new Insets(5));
        button.setAlignment(Pos.BOTTOM_CENTER);
        button.setTextAlignment(TextAlignment.CENTER);
        button.setId("backToMainMenuButton");
        return button;
    }

    public Button createResetButton() {
        Button button = new Button();
        button.setFont(Font.font("Monospaced", 20));
        button.setText("Reset game");
        button.setPadding(new Insets(5));
        button.setAlignment(Pos.BOTTOM_CENTER);
        button.setTextAlignment(TextAlignment.CENTER);
        button.setId("resetButton");
        return button;
    }

    public VBox createButtonsVBoxForGameView() {
        Label scoreLabel = new Label("X: " + game.getPointsX() + "  " + "O: " + game.getPointsO() + " \nNumber of winning symbols: " + game.getWinsOn());
        scoreLabel.setId("scoreLabel");
        labelTextAlign(scoreLabel);
        VBox vBox = new VBox();
        vBox.getChildren().add(scoreLabel);
        HBox hBox = new HBox();
        hBox.getChildren().add(resetButton);
        hBox.getChildren().add(backToMainMenuButton);
        hBox.setSpacing(300);
        hBox.setAlignment(Pos.CENTER);
        vBox.getChildren().add(hBox);
        return vBox;
    }

    public Scene createInputScene() {
        BorderPane inputLayout = new BorderPane();
        GridPane inputGrid = new GridPane();

        Label instructionsLabel = new Label("Please input the size of the playing board and the number of winning symbols.");
        inputLabelTextAlign(instructionsLabel);

        Label boardSizeLabel = new Label("Board size: ");
        TextField boardSizeTextField = new TextField();
        boardSizeTextField.setId("boardSizeTextField");
        Label boardSizeInfoLabel = new Label("Maximum 21");
        inputLabelTextAlign(boardSizeLabel);
        inputLabelTextAlign(boardSizeInfoLabel);

        Label numberOfWinningSymbolsLabel = new Label("Number of winning symbols: ");
        TextField numberOfWinningSymbolsTextField = new TextField();
        numberOfWinningSymbolsTextField.setId("numberOfWinningSymbolsTextField");
        Label numberOfWinningSymbolsInfoLabel = new Label("Must be lower or equal to board size.");
        inputLabelTextAlign(numberOfWinningSymbolsLabel);
        inputLabelTextAlign(numberOfWinningSymbolsInfoLabel);
        Button submitButton = new Button("Submit");
        submitButton.setId("submitButton");
        submitButton.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
        submitButton.setMaxWidth(80);

        HBox submitButtonHBox = new HBox();
        submitButtonHBox.setAlignment(Pos.CENTER_LEFT);
        submitButtonHBox.setPadding(new Insets(25, 10, 100, 100));
        submitButtonHBox.getChildren().addAll(submitButton);

        inputGrid.addRow(0, boardSizeLabel, boardSizeTextField, boardSizeInfoLabel);
        inputGrid.addRow(1, numberOfWinningSymbolsLabel, numberOfWinningSymbolsTextField, numberOfWinningSymbolsInfoLabel);

        inputLayout.setTop(instructionsLabel);
        inputLayout.setCenter(inputGrid);
        inputLayout.setBottom(submitButtonHBox);

        submitButton.setOnAction(e -> {
            try {
                int inputBoardSize = Integer.parseInt(boardSizeTextField.getText());
                int inputWinsOn = Integer.parseInt(numberOfWinningSymbolsTextField.getText());
                if (inputBoardSize <= 21 && inputBoardSize >= inputWinsOn && inputBoardSize >= 3 && inputWinsOn >= 3) {
                    stage.setScene(createGameScene(inputBoardSize, inputWinsOn));
                    stage.setMaximized(true);
                } else {
                    showInputError();
                }
            } catch (Exception exception) {
                showInputError();
            }
        });
        return new Scene(inputLayout, 600, 200);
    }

    public Scene createGameScene(int boardSize, int numberOfWinningSymbols) {
        game.createBoard(boardSize);
        game.setWinsOn(numberOfWinningSymbols);
        buttons = new Button[game.getBoardSize() * game.getBoardSize()];
        BorderPane gameLayout = new BorderPane();
        gameLayout.setId("gameLayout");
        GridPane gridPane = new GridPane();

        Label label = new Label("Turn: X");
        label.setId("startingTurnLabel");
        labelTextAlign(label);

        gameLayout.setTop(label);
        gameLayout.setCenter(gridPane);

        resetButton = createResetButton();
        reset(buttons, resetButton, gameLayout);
        backToMainMenuButton = createBackToMainMenuButton();
        backToMainMenu(buttons, backToMainMenuButton);

        gameLayout.setBottom(createButtonsVBoxForGameView());

        createGridOfButtons(gridPane, gameLayout);

        gridPane.setAlignment(Pos.CENTER);

        gridPane.setId("gameLayoutGridPane");
        return new Scene(gameLayout);
    }

    public void showInputError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error Dialog");
        alert.setHeaderText("The input is not valid.");
        alert.setContentText("Please set the Board size from 3 to 21 and the number of winning symbols has to be lower or equal to the Board size value. Minimum value for the number of winning symbols is 3.");
        alert.showAndWait();
    }

    public void createGridOfButtons(GridPane gridPane, BorderPane borderPane) {
        for (int i = 0; i < buttons.length; i++) {
            buttons[i] = new Button();
            buttons[i].setFont(Font.font("Monospaced", 20));
            buttons[i].setText(" ");
            buttons[i].setId("button" + i);
        }

        int buttonCounter = 0;
        for (int i = 0; i < game.getBoardSize(); i++) {
            for (int j = 0; j < game.getBoardSize(); j++) {
                gridPane.add(buttons[buttonCounter], i, j);
                clickGameButton(buttons[buttonCounter], borderPane, i, j);
                buttonCounter++;
            }
        }
    }
}