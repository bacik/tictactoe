package tictactoe;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import org.junit.*;
import org.testfx.framework.junit.ApplicationTest;
import org.testfx.service.query.EmptyNodeQueryException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class TicTacToeApplicationGameViewTest extends ApplicationTest {
    private final int boardSize = 10;
    private final int numberOfWinningSymbols = 5;

    @Override
    public void start(Stage stage) {
        TicTacToeApplication application = new TicTacToeApplication();
        application.start(stage);
        stage.setScene(application.createGameScene(boardSize, numberOfWinningSymbols));
    }

    @Test
    public void testButtonGrid() {
        List<Button> buttons = new ArrayList<>();
        try {
            for (int i = 0; i < (boardSize * boardSize) + 1; i++) {
                buttons.add(lookup("#button" + i).query());
            }
        } catch (EmptyNodeQueryException e) {
            System.out.println(buttons.size());
            assertEquals(boardSize * boardSize, buttons.size());
        }
    }

    @Test
    public void testClickButtonXSymbolAppears() {
        Button button = lookup("#button0").query();
        clickOn("#button0");
        assertEquals("X", button.getText());
    }

    @Test
    public void testClickButtonOSymbolAppears() {
        clickOn("#button0");
        Button button = lookup("#button1").query();
        clickOn("#button1");
        assertEquals("O", button.getText());
    }

    @Test
    public void testClickButtonChangesTurn() {
        clickOn("#button0");
        Label label = lookup("#turnLabel").query();
        assertEquals("Turn: O", label.getText());
    }

    @Test
    public void testClickButtonChangesTurnBack() {
        clickOn("#button0");
        clickOn("#button1");
        Label label = lookup("#turnLabel").query();
        assertEquals("Turn: X", label.getText());
    }

    @Test
    public void testClickButtonTwice() {
        Button button = lookup("#button0").query();
        clickOn("#button0");
        clickOn("#button0");
        assertEquals("X", button.getText());
    }

    @Test
    public void testClickButtonTwiceDoesntChangeTurn() {
        clickOn("#button0");
        clickOn("#button0");
        Label label = lookup("#turnLabel").query();
        assertEquals("Turn: O", label.getText());
    }

    @Test
    public void testFiveXWinsHorizontal() {
        for (int i = 0; i < 41; ) {
            if (i % 2 == 1) {
                clickOn("#button" + i);
                i += 9;
                continue;
            }
            clickOn("#button" + i);
            i++;
        }
        Label label = lookup("#gameOverLabel").query();
        assertEquals("Player X wins!", label.getText());
    }

    @Test
    public void testFiveOWinsHorizontal() {
        for (int i = 0; i < 40; ) {
            if (i % 2 == 1) {
                clickOn("#button" + i);
                i += 9;
                continue;
            }
            clickOn("#button" + i);
            i++;
        }
        clickOn("#button45");
        clickOn("#button41");
        Label label = lookup("#gameOverLabel").query();
        assertEquals("Player O wins!", label.getText());
    }

    @Test
    public void testFiveXWinsVertical() {
        for (int i = 0; i < 14; ) {
            if (i < 10) {
                clickOn("#button" + i);
                i += 10;
                continue;
            }
            clickOn("#button" + i);
            i -= 9;
        }
        Label label = lookup("#gameOverLabel").query();
        assertEquals("Player X wins!", label.getText());
    }

    @Test
    public void testFiveOWinsVertical() {
        for (int i = 0; i < 13; ) {
            if (i < 10) {
                clickOn("#button" + i);
                i += 10;
                continue;
            }
            clickOn("#button" + i);
            i -= 9;
        }
        clickOn("#button13");
        clickOn("#button16");
        clickOn("#button14");
        Label label = lookup("#gameOverLabel").query();
        assertEquals("Player O wins!", label.getText());
    }

    @Test
    public void testFiveXWinsDiagonal() {
        for (int i = 0; i < 45; ) {
            if (i % 11 == 0) {
                clickOn("#button" + i);
                i++;
                continue;
            }
            clickOn("#button" + i);
            i += 10;
        }
        Label label = lookup("#gameOverLabel").query();
        assertEquals("Player X wins!", label.getText());
    }

    @Test
    public void testFiveOWinsDiagonal() {
        clickOn("#button2");
        for (int i = 0; i < 45; ) {
            if (i % 11 == 0) {
                clickOn("#button" + i);
                i++;
                continue;
            }
            clickOn("#button" + i);
            i += 10;
        }
        Label label = lookup("#gameOverLabel").query();
        assertEquals("Player O wins!", label.getText());
    }

    @Test
    public void testFiveXWinsReverseDiagonal() {
        for (int i = 90; i > 53; ) {
            clickOn("#button" + i);
            clickOn("#button" + ++i);
            i -= 10;
        }
        Label label = lookup("#gameOverLabel").query();
        assertEquals("Player X wins!", label.getText());
    }

    @Test
    public void testFiveOWinsReverseDiagonal() {
        clickOn("#button2");
        for (int i = 90; i > 53; ) {
            clickOn("#button" + i);
            clickOn("#button" + ++i);
            i -= 10;
        }
        Label label = lookup("#gameOverLabel").query();
        assertEquals("Player O wins!", label.getText());
    }

    @Test
    public void testFiveXWinsScoreChangesForX() {
        for (int i = 0; i < 41; ) {
            if (i % 2 == 1) {
                clickOn("#button" + i);
                i += 9;
                continue;
            }
            clickOn("#button" + i);
            i++;
        }
        Label label = lookup("#scoreLabel").query();
        String[] labelText = label.getText().split(" ");
        assertEquals(1, Integer.parseInt(labelText[1]));
    }

    @Test
    public void testFiveXWinsScoreDoesntChangeForO() {
        for (int i = 0; i < 41; ) {
            if (i % 2 == 1) {
                clickOn("#button" + i);
                i += 9;
                continue;
            }
            clickOn("#button" + i);
            i++;
        }
        Label label = lookup("#scoreLabel").query();
        String[] labelText = label.getText().split(" ");
        assertEquals(0, Integer.parseInt(labelText[4]));
    }

    @Test
    public void testFiveOWinsScoreChangesForO() {
        clickOn("#button45");
        for (int i = 0; i < 41; ) {
            if (i % 2 == 1) {
                clickOn("#button" + i);
                i += 9;
                continue;
            }
            clickOn("#button" + i);
            i++;
        }
        Label label = lookup("#scoreLabel").query();
        String[] labelText = label.getText().split(" ");
        assertEquals(1, Integer.parseInt(labelText[4]));
    }

    @Test
    public void testFiveOWinsScoreDoesntChangeForX() {
        clickOn("#button45");
        for (int i = 0; i < 41; ) {
            if (i % 2 == 1) {
                clickOn("#button" + i);
                i += 9;
                continue;
            }
            clickOn("#button" + i);
            i++;
        }
        Label label = lookup("#scoreLabel").query();
        String[] labelText = label.getText().split(" ");
        assertEquals(0, Integer.parseInt(labelText[1]));
    }

    @Test
    public void testNumberOfWinningSymbolsCorrect() {
        Label label = lookup("#scoreLabel").query();
        String[] labelText = label.getText().split(" ");
        assertEquals(numberOfWinningSymbols, Integer.parseInt(labelText[labelText.length - 1]));
    }
}