package tictactoe;

import org.junit.*;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class GameTest {
    private Game game;
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        game = new Game();
        game.createBoard(10);
        game.setWinsOn(5);
    }

    public void changePlayerToO() {
        game.setPlayingPlayer(true);
        game.changeCurrentPlayerSymbol();
    }

    public void changePlayerToX() {
        game.setPlayingPlayer(false);
        game.changeCurrentPlayerSymbol();
    }

    @Test
    public void testAddSymbolOneXAdded() {
        game.addSymbol(0, 0);
        assertEquals("X", game.getBoard()[0][0]);
    }

    @Test
    public void testAddSymbolAddedToLastPossiblePlace() {
        game.addSymbol(9, 9);
        assertEquals("X", game.getBoard()[9][9]);
    }

    @Test
    public void testAddSymbolToTheWholeArray() {
        for (int i = 0; i < game.getBoardSize(); i++) {
            for (int j = 0; j < game.getBoardSize(); j++) {
                game.addSymbol(i, j);
            }
        }
        int counter = 0;
        for (int i = 0; i < game.getBoardSize(); i++) {
            for (int j = 0; j < game.getBoardSize(); j++) {
                if (game.getBoard()[i][j].equals("X")) {
                    counter++;
                }
            }
        }
        assertEquals(game.getBoardSize() * game.getBoardSize(), counter);
    }

    @Test
    public void testAddSymbolXAndOAdded() {
        game.setPlayingPlayer(true);
        game.changeCurrentPlayerSymbol();
        game.addSymbol(0, 0);
        assertEquals("O", game.getBoard()[0][0]);
    }

    @Test
    public void testAddSymbolOutOfBounds() {
        thrown.expect(IndexOutOfBoundsException.class);
        game.addSymbol(10, 10);
    }

    @Test
    public void testChangeCurrentSymbolO() {
        game.setPlayingPlayer(true);
        game.changeCurrentPlayerSymbol();
        assertEquals("O", game.getCurrentPlayerSymbol());
    }

    @Test
    public void testChangeCurrentSymbolX() {
        game.setPlayingPlayer(true);
        game.changeCurrentPlayerSymbol();
        game.setPlayingPlayer(false);
        game.changeCurrentPlayerSymbol();
        assertEquals("X", game.getCurrentPlayerSymbol());
    }

    @Test
    public void testResetBoardWithOneXSymbol() {
        game.addSymbol(0, 0);
        game.resetBoard();
        assertEquals("", game.getBoard()[0][0]);
    }

    @Test
    public void testResetBoardWithOneOSymbol() {
        game.setPlayingPlayer(true);
        game.changeCurrentPlayerSymbol();
        game.addSymbol(0, 0);
        game.resetBoard();
        assertEquals("", game.getBoard()[0][0]);
    }

    @Test
    public void testResetBoardWithWholeBoardFilledWithX() {
        for (int i = 0; i < game.getBoardSize(); i++) {
            for (int j = 0; j < game.getBoardSize(); j++) {
                game.addSymbol(i, j);
            }
        }
        game.resetBoard();
        int counter = 0;
        for (int i = 0; i < game.getBoardSize(); i++) {
            for (int j = 0; j < game.getBoardSize(); j++) {
                if (game.getBoard()[i][j].equals("X")) {
                    counter++;
                }
            }
        }
        assertEquals(counter, 0);
    }

    @Test
    public void testResetBoardWithWholeBoardFilledWithO() {
        game.setPlayingPlayer(true);
        game.changeCurrentPlayerSymbol();
        for (int i = 0; i < game.getBoardSize(); i++) {
            for (int j = 0; j < game.getBoardSize(); j++) {
                game.addSymbol(i, j);
            }
        }
        game.resetBoard();
        int counter = 0;
        for (int i = 0; i < game.getBoardSize(); i++) {
            for (int j = 0; j < game.getBoardSize(); j++) {
                if (game.getBoard()[i][j].equals("X")) {
                    counter++;
                }
            }
        }
        assertEquals(counter, 0);
    }

    @Test
    public void testPlayerWinCheckHorizontalXPositive() {
        for (int i = 0; i < 5; i++) {
            game.addSymbol(0, i);
        }
        assertTrue(game.playerWinCheck(0, 0));
    }

    @Test
    public void testPlayerWinCheckHorizontalXNegative() {
        for (int i = 0; i < 4; i++) {
            game.addSymbol(0, i);
        }
        assertFalse(game.playerWinCheck(0, 0));
    }

    @Test
    public void testPlayerWinCheckVerticalXPositive() {
        for (int i = 0; i < 5; i++) {
            game.addSymbol(i, 0);
        }
        assertTrue(game.playerWinCheck(0, 0));
    }

    @Test
    public void testPlayerWinCheckVerticalXNegative() {
        for (int i = 0; i < 4; i++) {
            game.addSymbol(i, 0);
        }
        assertFalse(game.playerWinCheck(0, 0));
    }

    @Test
    public void testPlayerWinCheckDiagonalXPositive() {
        for (int i = 0; i < 5; i++) {
            game.addSymbol(i, i);
        }
        assertTrue(game.playerWinCheck(0, 0));
    }

    @Test
    public void testPlayerWinCheckDiagonalXNegative() {
        for (int i = 0; i < 4; i++) {
            game.addSymbol(i, i);
        }
        assertFalse(game.playerWinCheck(0, 0));
    }

    @Test
    public void testPlayerWinCheckReverseDiagonalXPositive() {
        int j = 0;
        for (int i = 4; i >= 0; i--) {
            game.addSymbol(j, i);
            j++;
        }
        assertTrue(game.playerWinCheck(4, 0));
    }

    @Test
    public void testPlayerWinCheckReverseDiagonalXNegative() {
        int j = 0;
        for (int i = 4; i > 0; i--) {
            game.addSymbol(j, i);
            j++;
        }
        assertFalse(game.playerWinCheck(4, 0));
    }

    @Test
    public void testPlayerWinCheckHorizontalOPositive() {
        changePlayerToO();
        for (int i = 0; i < 5; i++) {
            game.addSymbol(0, i);
        }
        assertTrue(game.playerWinCheck(0, 0));
    }

    @Test
    public void testPlayerWinCheckHorizontalONegative() {
        changePlayerToO();
        for (int i = 0; i < 4; i++) {
            game.addSymbol(0, i);
        }
        assertFalse(game.playerWinCheck(0, 0));
    }

    @Test
    public void testPlayerWinCheckVerticalOPositive() {
        changePlayerToO();
        for (int i = 0; i < 5; i++) {
            game.addSymbol(i, 0);
        }
        assertTrue(game.playerWinCheck(0, 0));
    }

    @Test
    public void testPlayerWinCheckVerticalONegative() {
        changePlayerToO();
        for (int i = 0; i < 4; i++) {
            game.addSymbol(i, 0);
        }
        assertFalse(game.playerWinCheck(0, 0));
    }

    @Test
    public void testPlayerWinCheckDiagonalOPositive() {
        changePlayerToO();
        for (int i = 0; i < 5; i++) {
            game.addSymbol(i, i);
        }
        assertTrue(game.playerWinCheck(0, 0));
    }

    @Test
    public void testPlayerWinCheckDiagonalONegative() {
        changePlayerToO();
        for (int i = 0; i < 4; i++) {
            game.addSymbol(i, i);
        }
        assertFalse(game.playerWinCheck(0, 0));
    }

    @Test
    public void testPlayerWinCheckReverseDiagonalOPositive() {
        changePlayerToO();
        int j = 0;
        for (int i = 4; i >= 0; i--) {
            game.addSymbol(j, i);
            j++;
        }
        assertTrue(game.playerWinCheck(4, 0));
    }

    @Test
    public void testPlayerWinCheckReverseDiagonalONegative() {
        changePlayerToO();
        int j = 0;
        for (int i = 4; i > 0; i--) {
            game.addSymbol(j, i);
            j++;
        }
        assertFalse(game.playerWinCheck(4, 0));
    }

    @Test
    public void testTiePositive() {
        game.createBoard(3);
        game.setWinsOn(3);
        game.addSymbol(0, 0);
        game.addSymbol(0, 2);
        game.addSymbol(1, 0);
        game.addSymbol(2, 1);
        game.addSymbol(2, 2);
        changePlayerToO();
        game.addSymbol(0, 1);
        game.addSymbol(1, 1);
        game.addSymbol(1, 2);
        game.addSymbol(2, 0);
        game.playerWinCheck(2, 2);
        assertTrue(game.tieCheck());
    }

    @Test
    public void testTieNegative() {
        game.createBoard(3);
        game.setWinsOn(3);
        game.addSymbol(0, 0);
        game.addSymbol(0, 2);
        game.addSymbol(1, 2);
        game.addSymbol(2, 1);
        game.addSymbol(2, 2);
        changePlayerToO();
        game.addSymbol(0, 1);
        game.addSymbol(1, 1);
        game.addSymbol(1, 0);
        game.addSymbol(2, 0);
        changePlayerToX();
        game.addSymbol(2, 2);
        game.playerWinCheck(2, 2);
        assertFalse(game.tieCheck());
    }

    @Test
    public void testAddPointToXPositive() {
        game.createBoard(3);
        game.setWinsOn(3);
        game.addSymbol(0, 0);
        game.addSymbol(0, 1);
        game.addSymbol(0, 2);
        game.playerWinCheck(0, 2);
        assertEquals(1, game.getPointsX());
    }

    @Test
    public void testAddPointToXNegative() {
        game.createBoard(3);
        game.setWinsOn(3);
        game.addSymbol(0, 0);
        game.addSymbol(0, 1);
        game.addSymbol(2, 2);
        game.playerWinCheck(0, 2);
        game.playerWinCheck(0, 0);
        assertEquals(0, game.getPointsX());
    }

    @Test
    public void testAddPointToOPositive() {
        game.createBoard(3);
        game.setWinsOn(3);
        changePlayerToO();
        game.addSymbol(0, 0);
        game.addSymbol(0, 1);
        game.addSymbol(0, 2);
        game.playerWinCheck(0, 2);
        assertEquals(1, game.getPointsO());
    }

    @Test
    public void testAddPointToONegative() {
        game.createBoard(3);
        game.setWinsOn(3);
        changePlayerToO();
        game.addSymbol(0, 0);
        game.addSymbol(0, 1);
        game.addSymbol(2, 2);
        game.playerWinCheck(0, 2);
        game.playerWinCheck(0, 0);
        assertEquals(0, game.getPointsO());
    }

    @Test
    public void testAddPointToXAndNotToO() {
        game.createBoard(3);
        game.setWinsOn(3);
        game.addSymbol(0, 0);
        game.addSymbol(0, 1);
        game.addSymbol(0, 2);
        game.playerWinCheck(0, 2);
        assertEquals(1, game.getPointsX() - game.getPointsO());
    }

    @Test
    public void testAddPointToOAndNotToX() {
        game.createBoard(3);
        game.setWinsOn(3);
        changePlayerToO();
        game.addSymbol(0, 0);
        game.addSymbol(0, 1);
        game.addSymbol(0, 2);
        game.playerWinCheck(0, 2);
        assertEquals(1, game.getPointsO() - game.getPointsX());
    }

    @Test
    public void testResetScoreForOneToZero() {
        for (int i = 0; i < 5; i++) {
            game.addSymbol(0, i);
        }
        game.playerWinCheck(0, 0);
        int scoreXBeforeReset = game.getPointsX();
        game.resetScore();
        assertEquals(1, scoreXBeforeReset - game.getPointsO() - game.getPointsX());
    }

    @Test
    public void testResetScoreForZeroToOne() {
        changePlayerToO();
        for (int i = 0; i < 5; i++) {
            game.addSymbol(0, i);
        }
        game.playerWinCheck(0, 0);
        int scoreOBeforeReset = game.getPointsO();
        game.resetScore();
        assertEquals(1, scoreOBeforeReset - game.getPointsO() - game.getPointsX());
    }

    @Test
    public void testResetScoreForOneToOne() {
        for (int i = 0; i < 5; i++) {
            game.addSymbol(0, i);
        }
        game.playerWinCheck(0, 0);
        game.resetBoard();
        changePlayerToO();
        for (int i = 0; i < 5; i++) {
            game.addSymbol(0, i);
        }
        game.playerWinCheck(0, 0);
        int scoreTogetherBeforeReset = game.getPointsO() + game.getPointsX();
        game.resetScore();
        assertEquals(2, scoreTogetherBeforeReset - game.getPointsO() - game.getPointsX());
    }
}