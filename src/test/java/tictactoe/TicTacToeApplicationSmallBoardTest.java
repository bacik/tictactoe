package tictactoe;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import org.junit.*;
import org.testfx.framework.junit.ApplicationTest;

import static org.junit.Assert.*;

public class TicTacToeApplicationSmallBoardTest extends ApplicationTest {

    @Override
    public void start(Stage stage) {
        TicTacToeApplication application = new TicTacToeApplication();
        application.start(stage);
        stage.setScene(application.createGameScene(3, 3));
    }

    @Test
    public void testTie() {
        clickOn("#button0");
        clickOn("#button2");
        clickOn("#button1");
        clickOn("#button3");
        clickOn("#button6");
        clickOn("#button4");
        clickOn("#button5");
        clickOn("#button7");
        clickOn("#button8");
        Label label = lookup("#tieLabel").query();
        assertEquals("Tie!", label.getText());
    }

    @Test
    public void testResetBoardOneSymbol() {
        clickOn("#button0");
        Button button = lookup("#button0").query();
        clickOn("#resetButton");
        assertEquals(" ", button.getText());
    }

    @Test
    public void testResetBoardResetsTurnToX() {
        clickOn("#button0");
        clickOn("#resetButton");
        Label label = lookup("#turnLabel").query();
        assertEquals("Turn: X", label.getText());
    }

    @Test
    public void testResetFullBoard() {
        clickOn("#button0");
        clickOn("#button2");
        clickOn("#button1");
        clickOn("#button3");
        clickOn("#button6");
        clickOn("#button4");
        clickOn("#button5");
        clickOn("#button7");
        clickOn("#button8");
        clickOn("#resetButton");
        int counter = 0;
        for (int i = 0; i < 9; i++) {
            Button button = lookup("#button" + i).query();
            if (button.getText().equals(" ")) {
                counter++;
            }
        }
        assertEquals(9, counter);
    }

    @Test
    public void testBackToMainMenuButton() {
        clickOn("#button0");
        clickOn("#backToMainMenuButton");
        assertNotNull(lookup("#submitButton").query());
    }

    @Test
    public void testBackToMainMenuButtonCanGetBackToGame() {
        clickOn("#button0");
        Button button = lookup("#button0").query();
        clickOn("#backToMainMenuButton");
        clickOn("#boardSizeTextField");
        write("3");
        clickOn("#numberOfWinningSymbolsTextField");
        write("3");
        clickOn("#submitButton");
        sleep(1000);
        assertEquals(" ", button.getText());
    }

    @Test
    public void testBackToMainMenuButtonResetsTurn() {
        clickOn("#button0");
        clickOn("#backToMainMenuButton");
        clickOn("#boardSizeTextField");
        write("3");
        clickOn("#numberOfWinningSymbolsTextField");
        write("3");
        clickOn("#submitButton");
        sleep(1000);
        Label label = lookup("#startingTurnLabel").query();
        assertEquals("Turn: X", label.getText());
    }

    @Test
    public void testBackToMainMenuButtonResetsBoardSizeAndNumberOfWinningSymbols() {
        clickOn("#button0");
        clickOn("#backToMainMenuButton");
        clickOn("#boardSizeTextField");
        write("4");
        clickOn("#numberOfWinningSymbolsTextField");
        write("4");
        clickOn("#submitButton");
        sleep(1000);
        Label label = lookup("#scoreLabel").query();
        String[] labelText = label.getText().split(" ");
        assertEquals(4, Integer.parseInt(labelText[labelText.length - 1]));
    }
}