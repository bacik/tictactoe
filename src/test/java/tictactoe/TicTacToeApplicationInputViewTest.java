package tictactoe;

import javafx.stage.Stage;
import org.junit.*;
import org.testfx.framework.junit.ApplicationTest;
import org.testfx.service.query.EmptyNodeQueryException;

import static org.junit.Assert.*;

public class TicTacToeApplicationInputViewTest extends ApplicationTest {

    @Override
    public void start(Stage stage) {
        TicTacToeApplication application = new TicTacToeApplication();
        application.start(stage);
    }

    public void inputViewStartup(int boardSize, int numberOfWinningSymbols) {
        clickOn("#boardSizeTextField");
        write("" + boardSize);
        clickOn("#numberOfWinningSymbolsTextField");
        write("" + numberOfWinningSymbols);
        clickOn("#submitButton");
        sleep(1000);
    }

    @Test
    public void testCorrectHighestValues() {
        inputViewStartup(21, 21);
        try {
            lookup("#gameLayout").query();
        } catch (EmptyNodeQueryException e) {
            fail("GameLayout not found");
        }
    }

    @Test
    public void testCorrectBorderValues() {
        inputViewStartup(21, 3);
        try {
            lookup("#gameLayout").query();
        } catch (EmptyNodeQueryException e) {
            fail("GameLayout not found");
        }
    }

    @Test
    public void testLowestValues() {
        inputViewStartup(3, 3);
        try {
            lookup("#gameLayout").query();
        } catch (EmptyNodeQueryException e) {
            fail("GameLayout not found");
        }
    }

    @Test
    public void testNoValues() {
        clickOn("#submitButton");
        assertNotNull(lookup(".alert").query());
    }

    @Test
    public void testFirstValueIncorrectSecondCorrect() {
        inputViewStartup(22, 3);
        assertNotNull(lookup(".alert").query());
    }

    @Test
    public void testFirstValueCorrectSecondIncorrect() {
        inputViewStartup(21, 2);
        assertNotNull(lookup(".alert").query());
    }

    @Test
    public void testFirstValueIncorrectSecondEmpty() {
        clickOn("#boardSizeTextField");
        write("22");
        clickOn("#submitButton");
        sleep(1000);
        assertNotNull(lookup(".alert").query());
    }

    @Test
    public void testFirstValueEmptySecondIncorrect() {
        clickOn("#numberOfWinningSymbolsTextField");
        write("2");
        clickOn("#submitButton");
        sleep(1000);
        assertNotNull(lookup(".alert").query());
    }

    @Test
    public void testFirstValueCorrectSecondEmpty() {
        clickOn("#boardSizeTextField");
        write("3");
        clickOn("#submitButton");
        sleep(1000);
        assertNotNull(lookup(".alert").query());
    }

    @Test
    public void testFirstValueEmptySecondCorrect() {
        clickOn("#numberOfWinningSymbolsTextField");
        write("3");
        clickOn("#submitButton");
        sleep(1000);
        assertNotNull(lookup(".alert").query());
    }

    @Test
    public void testNotNumericalValues() {
        clickOn("#boardSizeTextField");
        write("twenty");
        clickOn("#numberOfWinningSymbolsTextField");
        write("two");
        clickOn("#submitButton");
        sleep(1000);
        assertNotNull(lookup(".alert").query());
    }

    @Test
    public void testSecondValueHigherThanFirst() {
        inputViewStartup(3, 4);
        assertNotNull(lookup(".alert").query());
    }
}